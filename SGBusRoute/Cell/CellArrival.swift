//
//  CellArrival.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 11/7/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit

class CellArrival: UITableViewCell {
    
    @IBOutlet weak var serviceNoLbl: UILabel!
    @IBOutlet weak var timeNextLbl: UILabel!
    @IBOutlet weak var timeSubqLbl: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadView: UIView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        activityIndicator.hidesWhenStopped = true
        loadView.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
