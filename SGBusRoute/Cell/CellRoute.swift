//
//  CellRoute.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 13/7/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit

class CellRoute: UITableViewCell {
    
    @IBOutlet weak var stopNameLbl: UILabel!
    @IBOutlet weak var stopNoLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
