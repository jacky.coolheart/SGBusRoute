//
//  Constants.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 28/7/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import Foundation

import UIKit

class Constants {
    static let DEBUG_MODE = true
    static let defaultDateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
}

class Strings {
    static let ok = "OK"
    static let oops = "Oops"
    static let invalidBusService = "We couldn't find the bus service you are looking for"
    static let invalidBusStop = "We couldn't find the bus stop you are looking for"
    static let NA = "N/A"
    static let Arr = "Arr"
}

class Endpoint {
    static let busArrival = "BusArrivalv2"
}

class Colors {
    static let controlShadowColor = UIColor(red: 142/255, green: 142/255, blue: 142/255, alpha: 1.0)
    static let separatorColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1.0)
    static let darkGreen = UIColor(red: 0, green: 100/255, blue: 0, alpha: 1.0)
    static let amber = UIColor(red: 1, green: 194/255, blue: 0, alpha: 1.0)
    static let gold = UIColor(red: 1, green: 215/255, blue: 0, alpha: 1.0)
    static let cyan = UIColor(red: 72/255, green: 187/255, blue: 236/255, alpha: 1.0)
    static let ios7Blue = UIColor(red:14.0/255, green:122.0/255, blue:254.0/255, alpha:1.0)
}

class AnnotationType {
    static let marker = "marker"
    static let stop = "stop"
    static let bus = "bus"
    static let busDD = "busDD"
    static let start = "start"
    static let end = "end"
}

class BusType {
    static let SingleDeck = "SD"
    static let DoubleDeck = "DD"
}
