//
//  Utility.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 12/7/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import Foundation

class Utility {

    static func dateFromString(string:String, withFormat format:String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: string)
    }
    
    static func stringFromDate(date:Date, withFormat format:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    static func getDiffMinutesFromNow(toDate date:Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: Date(), to: date).minute ?? 0
    }
}
