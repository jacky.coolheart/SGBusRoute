//
//  Network.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 19/6/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import RealmSwift

class Network: NSObject {
    
    static let shared = Network()

    let baseURL = "http://datamall2.mytransport.sg/ltaodataservice/"
    let headers: HTTPHeaders = [
        "Accept": "application/json",
        "AccountKey": "J+G1Onq0Ixiqx/1DqdlF1Q=="
    ]
    
    //https://blog.hyphe.me/realm-and-alamofire-in-a-effective-harmony/
    //https://stackoverflow.com/questions/28465706/how-to-find-my-realm-file
    
    //- Property 'BusStop.BusStopCode' has been changed from 'int' to 'string'., Error Code=10}
    //Consider database migration !! (21 June 2017)
    
    func getAllStops(skip:Int, completion: @escaping () -> Void) {

        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let parameters:[String:Any] = ["$skip":skip]
        Alamofire.request(baseURL + "BusStops", parameters: parameters, headers: headers).validate().responseArray(keyPath: "value") { (response: DataResponse<[BusStop]>) in
                        
            print("getAllStops, skip: \(skip)")
            
            switch response.result {
            case .success(let busStops):
                
                do {
                    let realm = try Realm()
                    try realm.write {
                        for busStop in busStops {
                            realm.add(busStop, update: true)
                        }
                    }
                    
                    if busStops.count == 0 {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        completion()
                    }
                    else {
                        self.getAllStops(skip: skip + 50, completion: completion)
                    }
                    
                } catch {
                    print("error writing to Realm database, error: \(error)")
                }

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getAllRoutes(skip:Int, completion: @escaping () -> Void) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let parameters:[String:Any] = ["$skip":skip]
        Alamofire.request(baseURL + "BusRoutes", parameters: parameters, headers: headers).validate().responseArray(keyPath: "value") { (response: DataResponse<[BusRoute]>) in
            
            print("getAllRoutes, skip: \(skip)")
            
            switch response.result {
            case .success(let routes):
                
                do {
                    let realm = try Realm()
                    try realm.write {
                        for route in routes {
                            
                            //get bus stop
                            if let realmObj = realm.object(ofType: BusStop.self, forPrimaryKey: route.BusStopCode) {
                                route.busStop = realmObj
                            }
                            
                            //set primary key
                            route.PrimaryKey = "\(route.ServiceNo)-\(route.Direction)-\(route.StopSequence)"
                            realm.add(route, update: true)
                        }
                    }
                    
                    if routes.count == 0 {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        completion()
                    }
                    else {
                        self.getAllRoutes(skip: skip + 50, completion: completion)
                    }
                    
                } catch {
                    print("error writing to Realm database, error: \(error)")
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getAllServices(skip:Int, completion: @escaping () -> Void) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let parameters:[String:Any] = ["$skip":skip]
        Alamofire.request(baseURL + "BusServices", parameters: parameters, headers: headers).validate().responseArray(keyPath: "value") { (response: DataResponse<[BusService]>) in
            
            print("getAllServices, skip: \(skip)")
            
            switch response.result {
            case .success(let busServices):
                
                do {
                    let realm = try Realm()
                    try realm.write {
                        for service in busServices {
                            realm.add(service, update: true)
                        }
                    }
                    
                    if busServices.count == 0 {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        completion()
                    }
                    else {
                        self.getAllServices(skip: skip + 50, completion: completion)
                    }
                    
                } catch {
                    print("error writing to Realm database, error: \(error)")
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func request(endpoint:String, parameters: Parameters, completion: @escaping (DataResponse<Any>) -> Void) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(baseURL + endpoint, parameters: parameters, headers: headers).validate().responseJSON { (response) in
            
            print("Network get response on thread: \(Thread.current) is main thread: \(Thread.isMainThread)")
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            completion(response)
        }
    }
}
