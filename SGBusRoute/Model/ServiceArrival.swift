//
//  ServiceArrival.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 10/7/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit
import Gloss

class ServiceArrival: Decodable {

    var ServiceNo: String! = ""
    var Operator: String! = ""
    var NextBus:Estimation?
    var NextBus2:Estimation?
    var NextBus3:Estimation?
    
    required init?(json: JSON) {
        self.ServiceNo = "ServiceNo" <~~ json
        self.Operator = "Operator" <~~ json
        self.NextBus = "NextBus" <~~ json
        self.NextBus2 = "NextBus2" <~~ json
        self.NextBus3 = "NextBus3" <~~ json
    }
}

extension ServiceArrival {
    
    static func fetch(onBusStopCode busStopID:String, serviceNo:String = "", completion: @escaping ([ServiceArrival]?, Error?) -> Void) {
        
        var params:[String:Any] = [:]
        if serviceNo.characters.count == 0 {
            params = [
                "BusStopCode":busStopID
            ]
        }
        else {
            params = [
                "BusStopCode":busStopID,
                "ServiceNo":serviceNo]
        }
        
        Network.shared.request(endpoint: Endpoint.busArrival, parameters: params) { (response) in
            
            switch response.result {
                case .success(let data):
                    if let dict = data as? [String:Any],
                        let jsonArray = dict["Services"] as? [JSON] {
                        
                        guard let arrivals = [ServiceArrival].from(jsonArray: jsonArray) else {
                            //TODO: handle error
                            //completion(nil, nil, composeError(domain: Constants.Error.domainData, code: 0, message: .localized(.errorJSONMappingDescription)))
                            return
                        }
                        
                        var arrivalIntArray:[ServiceArrival] = []
                        var arrivalStringArray:[ServiceArrival] = []
                        
                        for arrival in arrivals {
                            if let _ = Int(arrival.ServiceNo) {
                                arrivalIntArray.append(arrival)
                            } else {
                                arrivalStringArray.append(arrival)
                            }
                        }
                        
                        arrivalIntArray.sort(by: { (arr1, arr2) -> Bool in
                            if let no1 = Int(arr1.ServiceNo), let no2 = Int(arr2.ServiceNo) {
                                return no1 < no2
                            }
                            else {
                                return arr1.ServiceNo > arr2.ServiceNo
                            }
                        })
                        
                        arrivalStringArray.sort(by: { (arr1, arr2) -> Bool in
                            return arr1.ServiceNo < arr2.ServiceNo
                        })
                        
                        //TODO: enhance sorting !
                        var arrivalArray:[ServiceArrival] = arrivalIntArray
                        var arrIndex = 0
                        for arr in arrivalArray {
                            for arrLetter in arrivalStringArray {
                                if arrLetter.ServiceNo.hasPrefix(arr.ServiceNo) {
                                    arrivalArray.insert(arrLetter, at: arrIndex+1)
                                    if let index = arrivalStringArray.index(where: { $0.ServiceNo == arrLetter.ServiceNo}) {
                                        arrivalStringArray.remove(at: index)
                                    }
                                }
                            }
                            arrIndex += 1
                        }
                        arrivalArray.append(contentsOf: arrivalStringArray)
                        completion(arrivalArray, nil)
                }
                case .failure(let error):
                    completion(nil, error)
            }
        }
    }
}
