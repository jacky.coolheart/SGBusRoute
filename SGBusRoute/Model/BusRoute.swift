//
//  BusRoute.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 19/6/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import RealmSwift
import ObjectMapper

class BusRoute: Object, Mappable {
    dynamic var ServiceNo = ""
    dynamic var Operator = ""
    dynamic var Direction = 1
    dynamic var StopSequence = 1
    dynamic var BusStopCode = ""
    dynamic var Distance = 0.0
    dynamic var WD_FirstBus = ""
    dynamic var WD_LastBus = ""
    dynamic var SAT_FirstBus = ""
    dynamic var SAT_LastBus = ""
    dynamic var SUN_FirstBus = ""
    dynamic var SUN_LastBus = ""
    dynamic var PrimaryKey = ""
    dynamic var busStop:BusStop?
    
    override static func primaryKey() -> String? {
        return "PrimaryKey"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        ServiceNo <- map["ServiceNo"]
        Operator <- map["Operator"]
        Direction <- map["Direction"]
        StopSequence <- map["StopSequence"]
        BusStopCode <- map["BusStopCode"]
        Distance <- map["Distance"]
        WD_FirstBus <- map["WD_FirstBus"]
        WD_LastBus <- map["WD_LastBus"]
        SAT_FirstBus <- map["SAT_FirstBus"]
        SAT_LastBus <- map["SAT_LastBus"]
        SUN_FirstBus <- map["SUN_FirstBus"]
        SUN_LastBus <- map["SUN_LastBus"]
        //PrimaryKey = "\(ServiceNo)-\(Direction)-\(StopSequence)"
    }
}
