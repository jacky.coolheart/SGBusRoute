//
//  BusService.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 19/6/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import RealmSwift
import ObjectMapper

final class BusService: Object, Mappable {
    dynamic var ServiceNo = ""
    dynamic var Operator = ""
    dynamic var Direction = 1
    dynamic var Category = ""
    dynamic var OriginCode = ""
    dynamic var DestinationCode = ""
    dynamic var AM_Peak_Freq = ""
    dynamic var AM_Offpeak_Freq = ""
    dynamic var PM_Peak_Freq = ""
    dynamic var PM_Offpeak_Freq = ""
    dynamic var LoopDesc = ""
    
    override static func primaryKey() -> String? {
        return "ServiceNo"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        ServiceNo <- map["ServiceNo"]
        Operator <- map["Operator"]
        Direction <- map["Direction"]
        Category <- map["Category"]
        OriginCode <- map["OriginCode"]
        DestinationCode <- map["DestinationCode"]
        AM_Peak_Freq <- map["AM_Peak_Freq"]
        AM_Offpeak_Freq <- map["AM_Offpeak_Freq"]
        PM_Peak_Freq <- map["PM_Peak_Freq"]
        PM_Offpeak_Freq <- map["PM_Offpeak_Freq"]
        LoopDesc <- map["LoopDesc"]
    }
}
