//
//  BusStop.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 19/6/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import RealmSwift
import ObjectMapper

final class BusStop: Object, Mappable {
    dynamic var BusStopCode = ""
    dynamic var RoadName = ""
    dynamic var Description = ""
    dynamic var Latitude = 0.0
    dynamic var Longitude = 0.0
    
    override static func primaryKey() -> String? {
        return "BusStopCode"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        BusStopCode <- map["BusStopCode"]
        RoadName <- map["RoadName"]
        Description <- map["Description"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
    }
}
