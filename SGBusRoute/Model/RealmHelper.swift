//
//  RealmHelper.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 18/7/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import RealmSwift

class RealmHelper {

    static func getBusRoute(serviceNo:String, direction: Int) -> [BusRoute] {
        
        do {
            let realm = try Realm()
            let results = realm.objects(BusRoute.self).filter("ServiceNo = '\(serviceNo)' AND Direction = \(direction)").sorted(byKeyPath: "StopSequence", ascending: true)
            return Array(results)
            
        } catch {
            print(error)
        }
        
        return []
    }
    
    static func getBusStop(busStopID:String) -> BusStop? {
    
        do {
            let realm = try Realm()
            let results = realm.objects(BusStop.self).filter("BusStopCode = '\(busStopID)'")
            return results.first ?? nil
            
        } catch {
            print(error)
        }
        
        return nil
    }
    
    static func getBusService(serviceNo:String) -> BusService? {
        
        do {
            let realm = try Realm()
            let results = realm.objects(BusService.self).filter("ServiceNo = '\(serviceNo)'")
            return results.first ?? nil
            
        } catch {
            print(error)
        }
        
        return nil
    }
}
