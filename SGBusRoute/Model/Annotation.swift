//
//  Annotation.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 10/8/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit
import MapKit

class Annotation: NSObject, MKAnnotation {
    let title:String?
    let subtitle:String?
    let coordinate:CLLocationCoordinate2D
    let type:String
    var id:String
    
    init(title: String, subtitle: String, type:String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.type = type
        id = ""
        
        super.init()
    }
}
