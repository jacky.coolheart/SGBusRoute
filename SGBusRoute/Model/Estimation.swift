//
//  Estimation.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 10/7/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit
import Gloss

class Estimation: Decodable {
    
    var OriginCode: String! = ""
    var DestinationCode: String! = ""
    var EstimatedArrival:String! = ""
    var Latitude:String! = ""
    var Longitude:String! = ""
    var VisitNumber:String! = ""
    var Load:String! = ""
    var Feature:String! = ""
    var BusType:String! = ""
    
    required init?(json: JSON) {
        self.OriginCode = "OriginCode" <~~ json
        self.DestinationCode = "DestinationCode" <~~ json
        self.EstimatedArrival = "EstimatedArrival" <~~ json
        self.Latitude = "Latitude" <~~ json
        self.Longitude = "Longitude" <~~ json
        self.VisitNumber = "VisitNumber" <~~ json
        self.Load = "Load" <~~ json
        self.Feature = "Feature" <~~ json
        self.BusType = "Type" <~~ json
    }
}
