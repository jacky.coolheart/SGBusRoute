//
//  AppDelegate.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 15/6/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit
import RealmSwift
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    //fileprivate var currentSchemaVersion = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //https://realm.io/docs/swift/latest/#migrations
        let config = Realm.Configuration(
            schemaVersion: 2,
            migrationBlock: { migration, oldSchemaVersion in
            // potentially lengthy data migration
            
            if oldSchemaVersion < 2 {
               //Do nothing,migration logic here
                migration.enumerateObjects(ofType: BusRoute.className()) { oldObject, newObject in
                    print("")
                }
            }
        })
        Realm.Configuration.defaultConfiguration = config
        FirebaseApp.configure()
        
        if let udid = UIDevice.current.identifierForVendor?.uuidString {
            
            //Firebase db
            let dbRef = Database.database().reference()
            
            //udid
            dbRef.child("udid/\(udid)").observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.exists() {
                    //do nothing
                } else {
                    //entry does not exist
                    dbRef.child("udid/\(udid)").setValue(1)
                    
                    //phone model
                    let model = UIDevice.current.model
                    dbRef.child("phoneModel/\(model)").observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        if snapshot.exists() {
                            if let count = snapshot.value as? Int {
                                let newCount = count + 1
                                dbRef.child("phoneModel/\(model)").setValue(newCount)
                            }
                        } else {
                            //entry does not exist
                            dbRef.child("phoneModel/\(model)").setValue(1)
                        }
                        
                    }) { (error) in
                        print(error.localizedDescription)
                    }
                    
                    //phone OS
                    let systemVersion = UIDevice.current.systemVersion.replacingOccurrences(of: ".", with: "_")
                    dbRef.child("phoneOS/\(systemVersion)").observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        if snapshot.exists() {
                            if let count = snapshot.value as? Int {
                                let newCount = count + 1
                                dbRef.child("phoneOS/\(systemVersion)").setValue(newCount)
                            }
                        } else {
                            //entry does not exist
                            dbRef.child("phoneOS/\(systemVersion)").setValue(1)
                        }
                        
                    }) { (error) in
                        print(error.localizedDescription)
                    }
                    
                    //locale
                    if let locale = (Locale.current as NSLocale).object(forKey: .identifier) as? String {
                        dbRef.child("locale/\(locale)").observeSingleEvent(of: .value, with: { (snapshot) in
                            
                            if snapshot.exists() {
                                if let count = snapshot.value as? Int {
                                    let newCount = count + 1
                                    dbRef.child("locale/\(locale)").setValue(newCount)
                                }
                            } else {
                                //entry does not exist
                                dbRef.child("locale/\(locale)").setValue(1)
                            }
                            
                        }) { (error) in
                            print(error.localizedDescription)
                        }
                    }
                    
                }//end if udid does not exist
                
            }) { (error) in
                print(error.localizedDescription)
            }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
}

