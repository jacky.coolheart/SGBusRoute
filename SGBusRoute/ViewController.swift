//
//  ViewController.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 15/6/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift
import SearchTextField
import Firebase

//TODO:
//6. Dismiss keyboard on Map tap

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var mapBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var segmentContainer: UIView!
    @IBOutlet weak var segmentTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblDownloading: UILabel!
    @IBOutlet weak var viewDownloadingBottomConstraint: NSLayoutConstraint!
    
    fileprivate let placeHolderBusStop = "Bus stop code or Bus stop name"
    fileprivate let placeholderServiceNo = "Bus number"
    fileprivate var inputTF:SearchTextField!
    fileprivate var panelState:PanelState = .Default
    fileprivate var panelCompactHeight:CGFloat = 50
    fileprivate let segmentDefaultTopConstraintValue:CGFloat = -44

    enum Segment:Int {
        case BusStop = 0
        case ServiceNo
    }
    
    enum PanelState:Int {
        case Default = 0
        case Full
        case Compact
    }

    fileprivate lazy var listArrivalController:ListArrivalViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "ListArrivalViewController") as! ListArrivalViewController
        vc.delegate = self
        return vc
    }()

    fileprivate lazy var listRouteController:ListRouteViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "ListRouteViewController") as! ListRouteViewController
        vc.delegate = self
        return vc
    }()
    
    fileprivate lazy var busStopDetailController:BusStopDetailViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "BusStopDetailViewController") as! BusStopDetailViewController
        return vc
    }()
    
    fileprivate lazy var serviceDetailController:ServiceDetailViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "ServiceDetailViewController") as! ServiceDetailViewController
        return vc
    }()
    
    fileprivate lazy var realm:Realm? = {
        do {
            return try Realm()
        }
        catch {
            //TODO: alert
        }
        return nil
    }()
    
    fileprivate var listOffsetY:CGFloat = 0
    fileprivate var listOffsetYRoute:CGFloat = 0
    fileprivate var firebaseDBRef:DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        segmentControl.tintColor = .white
        segmentControl.backgroundColor = .darkGray
        segmentControl.layer.cornerRadius = 7
        segmentControl.layer.borderColor = UIColor.clear.cgColor
        
        //uitextfield
        let fontSize:CGFloat = 14
        let rect = CGRect(x: 0, y: 0, width: view.frame.width, height: 30)
        inputTF = SearchTextField(frame: rect)
        inputTF.delegate = self
        inputTF.backgroundColor = .white
        inputTF.layer.cornerRadius = 5
        inputTF.clearButtonMode = .whileEditing
        inputTF.returnKeyType = .search
        inputTF.placeholder = placeHolderBusStop
        inputTF.font = UIFont.systemFont(ofSize: fontSize)
        inputTF.maxNumberOfResults = 10 //SearchTextField
        inputTF.theme.bgColor = UIColor.white
        inputTF.theme.font = UIFont.systemFont(ofSize: fontSize)
        inputTF.highlightAttributes = [NSFontAttributeName:UIFont.boldSystemFont(ofSize: fontSize)]
        inputTF.theme.separatorColor = UIColor.lightGray
        inputTF.theme.borderColor = UIColor.lightGray
        inputTF.theme.cellHeight = 44
        inputTF.itemSelectionHandler = { filteredResults, itemPosition in
            let item = filteredResults[itemPosition]
            self.inputTF.text = item.title
            self.inputTF.resignFirstResponder()
        }

        navigationItem.titleView = inputTF
        
        //list
        //listOffsetY = view.bounds.height - 250 + UIApplication.shared.statusBarFrame.height
        listOffsetY = view.bounds.height - (50 + 47 * 5)
        listOffsetYRoute = listOffsetY - 50
        self.title = Bundle.main.infoDictionary![kCFBundleNameKey as String] as? String
        
        //overlay
        overlayView.isHidden = true
    
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(overlayTapped(tapGesture:)))
        overlayView.addGestureRecognizer(tapGesture)
        
        //map
        //mapView.showsUserLocation = true
        
        //navigation
        navigationController?.navigationBar.backgroundColor = UIColor.groupTableViewBackground
        navigationController?.navigationBar.isOpaque = false
        
        //Firebase
        firebaseDBRef = Database.database().reference()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        populateDatabase()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

// MARK: - UIGestureRecognizer

extension ViewController {
    
    internal func overlayTapped(tapGesture:UITapGestureRecognizer) {
        inputTF.resignFirstResponder()
    }
}


// MARK: - IBActions

extension ViewController {
    
    @IBAction func segmentDidChange(_ sender: AnyObject) {
        
        listArrivalController.view.removeFromSuperview()
        listRouteController.view.removeFromSuperview()
        
        self.mapBottomConstraint.constant = 0
        self.mapView.removeOverlays(self.mapView.overlays)
        mapView.removeAnnotations(mapView.annotations)
        
        if segmentControl.selectedSegmentIndex == Segment.BusStop.rawValue {
            inputTF.placeholder = placeHolderBusStop
        }
        else if segmentControl.selectedSegmentIndex == Segment.ServiceNo.rawValue {
            inputTF.placeholder = placeholderServiceNo
        }
        
        inputTF.text = ""
    }
}

// MARK: - Helpers

//TextField with suggestion list
//https://github.com/apasccon/SearchTextField

extension ViewController {

    func populateDatabase() {
        
        self.viewDownloadingBottomConstraint.constant = -(self.lblDownloading.bounds.height)
    
        do {
            let realm = try Realm()
            let stops = realm.objects(BusStop.self)
            //4920 stops (as of 21 June 2017), skip: 4950
            //20117 route entries (as of 22 June 2017), skip: 24200
            
            if stops.count < 1000 {
                
                // bus stops
                lblDownloading.text = "Downloading bus stops data..."
                viewDownloadingBottomConstraint.constant = 0
                
                Network.shared.getAllStops(skip: 0) {
                    print("fetch all stops completed")
                    
                    // proceed to fetch bus services
                    let services = realm.objects(BusService.self)
                    if services.count < 1000 {
                        
                        self.lblDownloading.text = "Downloading bus services data..."
                        
                        Network.shared.getAllServices(skip: 0, completion: {
                            print("fetch all services completed")
                            
                            //proceed to fetch bus routes
                            self.lblDownloading.text = "Downloading bus routes data..."
                            
                            Network.shared.getAllRoutes(skip: 0, completion: {
                                print("fetch all routes completed")
                                
                                UIView.animate(withDuration: 0.25, animations: {
                                    self.viewDownloadingBottomConstraint.constant = -(self.lblDownloading.bounds.height)
                                })
                            })
                        })
                    }
                }
            } else {
                
                // bus services
                let services = realm.objects(BusService.self)
                if services.count < 1000 {
                    
                    lblDownloading.text = "Downloading bus services data..."
                    viewDownloadingBottomConstraint.constant = 0
                    
                    Network.shared.getAllServices(skip: 0, completion: {
                        print("fetch all services completed")
                        self.viewDownloadingBottomConstraint.constant = -(self.lblDownloading.bounds.height)
                        
                        // bus routes
                        let routes = realm.objects(BusRoute.self)
                        if routes.count < 2000 {
                            
                            self.lblDownloading.text = "Downloading bus routes data..."
                            self.viewDownloadingBottomConstraint.constant = 0
                            
                            Network.shared.getAllRoutes(skip: 0, completion: {
                                self.lblDownloading.text = ""
                                print("fetch all routes completed")
                                
                                UIView.animate(withDuration: 0.25, animations: {
                                    self.viewDownloadingBottomConstraint.constant = -(self.lblDownloading.bounds.height)
                                })
                            })
                        }
                    })
                }
            }
            
        } catch {
            print(error)
        }
    }
    
    func displayArrivals(_ busStopCode:String, serviceNo: String) {
        
        //fetch from network
        if busStopCode.characters.count == 0 {
            return
        }
        
        if serviceNo.characters.count > 0 {
            //show loading indicator on cell
            if self.listArrivalController.view.superview != nil {
                self.listArrivalController.showLoading(serviceNo: serviceNo)
            }
        }

        ServiceArrival.fetch(onBusStopCode: busStopCode, serviceNo: serviceNo) { (arrivalArray, error) in
            
            guard let arrivalArray = arrivalArray, arrivalArray.count > 0 else {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: Strings.oops,
                                                  message: Strings.invalidBusStop,
                                                  preferredStyle: .alert)
                    let action = UIAlertAction(title: Strings.ok, style: .default, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                return
            }
            
            //Firebase db
            self.firebaseDBRef.child("busStop/\(busStopCode)").observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.exists() {
                    if let count = snapshot.value as? Int {
                        let newCount = count + 1
                        self.firebaseDBRef.child("busStop/\(busStopCode)").setValue(newCount)
                    }
                } else {
                    //entry does not exist
                    self.firebaseDBRef.child("busStop/\(busStopCode)").setValue(1)
                }
                
            }) { (error) in
                print(error.localizedDescription)
            }
            
            if let busStop = RealmHelper.getBusStop(busStopID: busStopCode) {
                
                if serviceNo.characters.count == 0 {
                    
                    //update the whole table
                    DispatchQueue.main.async {
                        if self.listArrivalController.view.superview == nil {
                            var frame = self.view.frame
                            frame.origin = CGPoint(x: 0, y: self.listOffsetY)
                            frame.size.height = frame.height - self.listOffsetY
                            self.listArrivalController.view.frame = frame
                            self.view.addSubview(self.listArrivalController.view)
                            self.addChildViewController(self.listArrivalController)
                        }
                        self.listArrivalController.populateArrivals(arrivals: arrivalArray, onBusStop: busStop)
                        self.mapBottomConstraint.constant = self.listArrivalController.view.bounds.height
                        
                        //map view
                        let coordinate = CLLocationCoordinate2D(latitude: busStop.Latitude, longitude: busStop.Longitude)
                        
                        //clear annotations & overlays
                        self.mapView.removeAnnotations(self.mapView.annotations)
                        self.mapView.removeOverlays(self.mapView.overlays)
                        
                        //add anntation
                        let annotation = Annotation(title: busStop.Description,
                                                    subtitle: "\(busStop.BusStopCode), \(busStop.RoadName)",
                            type: AnnotationType.marker,
                            coordinate: coordinate)
                        self.mapView.addAnnotation(annotation)
                        self.mapView.selectAnnotation(annotation, animated: true)
                        self.mapView.reloadInputViews()
                        
                        let adjustedCoordinate = CLLocationCoordinate2DMake(coordinate.latitude + 0.0007, coordinate.longitude)
                        let region = MKCoordinateRegionMakeWithDistance(adjustedCoordinate, 400, 400)
                        let adjustedRegion = self.mapView.regionThatFits(region)
                        self.mapView.setRegion(adjustedRegion, animated: true)
                    }
                }//end if count > 0
                else {
                    
                    self.listArrivalController.hideLoading(serviceNo: serviceNo)
                
                    //update a single cell
                    if arrivalArray.count == 1 {
                        let arrival = arrivalArray[0]
                        
                        if let nextBus = arrival.NextBus {
                            if let nextBusLatitude = CLLocationDegrees(nextBus.Latitude), let nextBusLongitude = CLLocationDegrees(nextBus.Longitude) {
                                
                                let centerCoordinate = CLLocationCoordinate2DMake(abs(busStop.Latitude + nextBusLatitude) * 0.5, abs(busStop.Longitude + nextBusLongitude) * 0.5)
                                
                                //clear callout & annotations
                                let mapView = self.mapView!
                                for annotation in mapView.annotations {
                                    
                                    if annotation is MKPointAnnotation {
                                        mapView.removeAnnotation(annotation)
                                    }
                                    else if annotation is Annotation {
                                        if let type = (annotation as? Annotation)?.type {
                                            if type == AnnotationType.marker {
                                                mapView.deselectAnnotation(annotation, animated: true)
                                            }
                                            else if type == AnnotationType.bus || type == AnnotationType.busDD {
                                                mapView.removeAnnotation(annotation)
                                            }
                                        }
                                    }
                                }
                                
                                //clear overlays
                                self.mapView.removeOverlays(self.mapView.overlays)
                                
                                if Constants.DEBUG_MODE {
                                    let centerAnnotation = MKPointAnnotation()
                                    
                                    centerAnnotation.coordinate = centerCoordinate
                                    centerAnnotation.title = "Center Point"
                                    mapView.addAnnotation(centerAnnotation)
                                }
                                
                                //bus timing
                                var nextBusEstMins = ""
                                if let nextBusArrString = arrival.NextBus?.EstimatedArrival {
                                    if let nextBusArrDate = Utility.dateFromString(string: nextBusArrString, withFormat: Constants.defaultDateFormat) {
                                        let nextBusArrMins = Utility.getDiffMinutesFromNow(toDate: nextBusArrDate)
                                        nextBusEstMins = nextBusArrMins <= 0 ? Strings.Arr : "\(nextBusArrMins) mins away"
                                    }
                                    else {
                                        nextBusEstMins = Strings.NA
                                    }
                                }
                                
                                var annotationType = AnnotationType.bus
                                if arrival.NextBus?.BusType == BusType.DoubleDeck {
                                    annotationType = AnnotationType.busDD
                                }
                                
                                let busCoordinate = CLLocationCoordinate2DMake(nextBusLatitude, nextBusLongitude)
                                let busAnnotation = Annotation(title: arrival.ServiceNo, subtitle: nextBusEstMins,
                                                               type: annotationType, coordinate: busCoordinate)
                                mapView.addAnnotation(busAnnotation)
                                mapView.selectAnnotation(busAnnotation, animated: true)
                                
                                //add route
                                var coordinates:[CLLocationCoordinate2D] = []
                                coordinates.append(CLLocationCoordinate2DMake(busStop.Latitude, busStop.Longitude))
                                coordinates.append(busCoordinate)
                                
                                let polyline = MKPolyline(coordinates: coordinates, count: coordinates.count)
                                self.mapView.add(polyline)
                                
                                //map to include all annotation points
                                mapView.showAnnotations(mapView.annotations, animated: true)
                            }
                        }
                    }
                }//end else
            }//end if busStop retrieved
        }
    }
    
    func displayRoutes(serviceNo:String, direction: Int) {
        
        //fetch from database
        let routes = RealmHelper.getBusRoute(serviceNo: serviceNo, direction: direction)
        
        if routes.count == 0 {
            let alert = UIAlertController(title: Strings.oops,
                                          message: Strings.invalidBusService,
                                          preferredStyle: .alert)
            let action = UIAlertAction(title: Strings.ok, style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
        } else {
    
            //Firebase db
            self.firebaseDBRef.child("service/\(serviceNo)").observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.exists() {
                    if let count = snapshot.value as? Int {
                        let newCount = count + 1
                        self.firebaseDBRef.child("service/\(serviceNo)").setValue(newCount)
                    }
                } else {
                    //entry does not exist
                    self.firebaseDBRef.child("service/\(serviceNo)").setValue(1)
                }
                
            }) { (error) in
                print(error.localizedDescription)
            }
            
            
            if listRouteController.view.superview == nil {
                var frame = self.view.frame
                frame.origin = CGPoint(x: 0, y: self.listOffsetYRoute)
                frame.size.height = frame.height - self.listOffsetYRoute
                self.listRouteController.view.frame = frame
                
                self.view.addSubview(self.listRouteController.view)
            }
            listRouteController.populateRoutes(routes, serviceNo: serviceNo, direction: direction)
            mapBottomConstraint.constant = listRouteController.view.bounds.height
            
            //clear annotations & overlays
            mapView.removeAnnotations(mapView.annotations)
            mapView.removeOverlays(mapView.overlays)
            
            //add start & end route
            var coordinates:[CLLocationCoordinate2D] = []
           
            for i in 0 ..< routes.count {
                
                let route = routes[i]
                if let busStop = route.busStop {
                    
                    let coordinate = CLLocationCoordinate2D(latitude: busStop.Latitude, longitude: busStop.Longitude)
                    coordinates.append(coordinate)
                    
                    if i == 0 {
                        let startAnnotation = Annotation(title: busStop.Description, subtitle: busStop.BusStopCode,
                                                       type: AnnotationType.start, coordinate: coordinate)
                        startAnnotation.id = busStop.BusStopCode
                        mapView.addAnnotation(startAnnotation)
                        
                    } else if i == routes.count - 1 {
                        let endAnnotation = Annotation(title: busStop.Description, subtitle: busStop.BusStopCode,
                                                         type: AnnotationType.end, coordinate: coordinate)
                        endAnnotation.id = busStop.BusStopCode
                        mapView.addAnnotation(endAnnotation)
                        
                    } else {
                    
                        let dotAnnotation = Annotation(title: busStop.Description, subtitle: busStop.BusStopCode,
                                                       type: AnnotationType.stop, coordinate: coordinate)
                        dotAnnotation.id = busStop.BusStopCode
                        mapView.addAnnotation(dotAnnotation)
                    }
                }
            }
            
            //add route
            let polyline = MKPolyline(coordinates: coordinates, count: coordinates.count)
            mapView.add(polyline)
            mapView.showAnnotations(mapView.annotations, animated: true)
        }
    }
}

// MARK: - UITextFieldDelegate

extension ViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.3) {
            
            //overlay
            self.overlayView.isHidden = false
            self.overlayView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            //segment
            self.segmentTopConstraint.constant = 0
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let input = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            
            //clear overlay
            self.overlayView.isHidden = true
            
            //segment
            self.segmentTopConstraint.constant = segmentDefaultTopConstraintValue
            
            if input.characters.count == 0 {
                return
            }
            
            if segmentControl.selectedSegmentIndex == Segment.BusStop.rawValue {
                listRouteController.view.removeFromSuperview()
                displayArrivals(input, serviceNo: "")
            }
            else if segmentControl.selectedSegmentIndex == Segment.ServiceNo.rawValue {
                listArrivalController.view.removeFromSuperview()
                displayRoutes(serviceNo: input, direction: 1)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let textFieldText = (textField.text ?? "") as NSString
        let newText = textFieldText.replacingCharacters(in: range, with: string)
        
        if segmentControl.selectedSegmentIndex == Segment.BusStop.rawValue {
            
            var predicateFormat = "BusStopCode BEGINSWITH[c] %@"
            
            if let firstCharacter = newText.characters.first {
                if !String(firstCharacter).isNumber() {
                    //is not number !
                    predicateFormat = "Description CONTAINS[c] %@"
                }
            }
            
            if let busStops = realm?.objects(BusStop.self).filter(predicateFormat, newText) {
                var suggestions:[SearchTextFieldItem] = []
                for busStop in Array(busStops) {
                    let item = SearchTextFieldItem(title: busStop.BusStopCode, subtitle: busStop.Description)
                    suggestions.append(item)
                }

                inputTF.filterItems(suggestions)
            }
        }
        else if segmentControl.selectedSegmentIndex == Segment.ServiceNo.rawValue {
            
            if let busRoutes = realm?.objects(BusRoute.self).filter("ServiceNo BEGINSWITH[c] %@ AND Direction = 1 AND StopSequence = 1", newText) {
                var suggestions:[SearchTextFieldItem] = []
                for busRoute in Array(busRoutes) {
                    let item = SearchTextFieldItem(title: busRoute.ServiceNo, subtitle: busRoute.Operator)
                    suggestions.append(item)
                }

                inputTF.filterItems(suggestions)
            }
        }

        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - MKMapViewDelegate

extension ViewController: MKMapViewDelegate {

    //https://stackoverflow.com/questions/38599534/how-to-make-map-pins-animate-drop-swift-2-2-ios-9
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var markerView: MKAnnotationView? = nil
        
        if annotation is MKPointAnnotation {
            
            let identifier = "debug"
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                dequeuedView.annotation = annotation
                markerView = dequeuedView
            } else {
                let pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                pinView.pinTintColor = .yellow
                
                markerView = pinView
            }
        }
        else if annotation is Annotation {
            
            let ann = annotation as! Annotation
            
            //TODO: pin is changing color if done this way...
            if ann.type == AnnotationType.start || ann.type == AnnotationType.end {
                let identifier = "startEndAnnotation"
                if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                    dequeuedView.annotation = annotation
                    markerView = dequeuedView
                } else {
                    markerView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                }
                
                if markerView is MKPinAnnotationView {
                    let pinView = markerView as! MKPinAnnotationView
                    if ann.type == AnnotationType.start {
                        pinView.pinTintColor = .green
                    }
                    else if ann.type == AnnotationType.end {
                        pinView.pinTintColor = .red
                    }
                }
            }
            else {
                let identifier = "annotation"
                if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                    dequeuedView.annotation = annotation
                    markerView = dequeuedView
                }
                else {
                    markerView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                }
            }
        
            if ann.type == AnnotationType.marker {
                markerView?.image = UIImage(named: "marker")
            }
            else if ann.type == AnnotationType.bus {
                markerView?.image = UIImage(named: "bus")
            }
            else if ann.type == AnnotationType.busDD {
                markerView?.image = UIImage(named: "busDD")
            }
            else if ann.type == AnnotationType.stop {
                markerView?.image = UIImage(named: "dot")
                //markerView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            
        }//end else if
        
        markerView?.canShowCallout = true
        
        return markerView
    }
    
    //https://stackoverflow.com/questions/16838360/ios-sdk-mapkit-mkpolyline-not-showing
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
            renderer.fillColor = UIColor.red
            renderer.strokeColor = Colors.cyan
            renderer.lineWidth = 5.0
            return renderer
        }
        
        return MKOverlayRenderer()
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if view.reuseIdentifier == "annotation" {
            print("accessory tapped")
            self.present(busStopDetailController, animated: true, completion: nil)
        }
    }
}

// MARK: - ListArrivalViewControllerDelegate

extension ViewController: ListArrivalViewControllerDelegate {
    
    func didTapRefresh(onBusStop busStop: BusStop) {
        displayArrivals(busStop.BusStopCode, serviceNo: "")
    }
    
    func didTapArrivalRow(withArrival arr: ServiceArrival, onBusStop busStop: BusStop) {
        displayArrivals(busStop.BusStopCode, serviceNo: arr.ServiceNo)
    }

    func arrivalListDidSwipe(direction: UISwipeGestureRecognizerDirection) {

        var yValue = self.listOffsetY
        var height:CGFloat = 0
        var mapHeight = self.mapView.frame.height
        
        UIView.animate(withDuration: 0.5, animations: {
            
            var frame = self.listArrivalController.view.frame
            var mapFrame = self.mapView.frame
            
            if self.panelState == .Default {
                if direction == .up {
                    yValue = 0
                    height = self.view.frame.height
                } else if direction == .down {
                    yValue = self.view.frame.height - self.panelCompactHeight
                    height = self.panelCompactHeight
                    mapHeight = self.view.frame.height - self.panelCompactHeight
                    self.mapBottomConstraint.constant = 0
                }
            }
            else if self.panelState == .Full {
                if direction == .down {
                    yValue = self.listOffsetY
                    height = self.view.frame.height - self.listOffsetY
                }
            }
            else if self.panelState == .Compact {
                if direction == .up {
                    yValue = self.listOffsetY
                    height = self.view.frame.height - self.listOffsetY
                    mapHeight = self.view.frame.height - self.listArrivalController.view.bounds.height
                    self.mapBottomConstraint.constant = self.listArrivalController.view.bounds.height
                }
            }
            
            frame.origin.y = yValue
            frame.size.height = height
            self.listArrivalController.view.frame = frame
            
            mapFrame.size.height = mapHeight
            self.mapView.frame = mapFrame
            
        }) { (finished) in
            //change state
            if self.panelState == .Default {
                if direction == .up {
                    self.panelState = .Full
                } else if direction == .down {
                    self.panelState = .Compact
                }
            }
            else if self.panelState == .Full {
                if direction == .down {
                    self.panelState = .Default
                }
            }
            else if self.panelState == .Compact {
                if direction == .up {
                    self.panelState = .Default
                    self.mapBottomConstraint.constant = self.listArrivalController.view.bounds.height
                }
            }
        }
    }
}

// MARK: - ListRouteViewControllerDelegate

extension ViewController: ListRouteViewControllerDelegate {
    
    func didTapMapIcon() {
        mapView.annotations.forEach({ mapView.deselectAnnotation($0, animated: true) })
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
    
    func didTapServiceDetail(onBusService busService: BusService) {
        serviceDetailController.busService = busService
        navigationController?.pushViewController(serviceDetailController, animated: true)
    }
    
    func didTapRouteRow(withBusStop busStop: BusStop) {
        
        if let annotations = mapView.annotations as? [Annotation] {
            let resMarker = annotations.filter({ $0.type == AnnotationType.marker })
            mapView.removeAnnotations(resMarker)
            
            let resAnn = annotations.filter({ $0.id == busStop.BusStopCode })
            if let ann = resAnn.first {
                mapView.selectAnnotation(ann, animated: true)
            }
        }
        
        let coordinate = CLLocationCoordinate2DMake(busStop.Latitude, busStop.Longitude)
        let adjustedCoordinate = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        let region = MKCoordinateRegionMakeWithDistance(adjustedCoordinate, 1500, 1500)
        let adjustedRegion = mapView.regionThatFits(region)
        mapView.setRegion(adjustedRegion, animated: true)
    }
    
    func routeSegmentDidChange(onServiceNo serviceNo: String, toDirection direction: Int) {
        mapView.annotations.forEach({ mapView.deselectAnnotation($0, animated: true) })//TODO: to enhance
        displayRoutes(serviceNo: serviceNo, direction: direction)
    }
    
    func routeListDidSwipe(direction: UISwipeGestureRecognizerDirection) {
        
        var yValue = self.listOffsetYRoute
        var height:CGFloat = self.listRouteController.view.bounds.height
        var mapHeight = self.mapView.frame.height
        
        UIView.animate(withDuration: 0.5, animations: {
            
            var frame = self.listArrivalController.view.frame
            var mapFrame = self.mapView.frame
            
            if self.panelState == .Default {
                if direction == .up {
                    yValue = 0
                    height = self.view.frame.height
                } else if direction == .down {
                    yValue = self.view.frame.height - self.panelCompactHeight
                    mapHeight = self.view.frame.height - self.panelCompactHeight
                    self.mapBottomConstraint.constant = 0
                }
            }
            else if self.panelState == .Full {
                if direction == .down {
                    yValue = self.listOffsetYRoute
                    height = self.view.frame.height - self.listOffsetYRoute
                }
            }
            else if self.panelState == .Compact {
                if direction == .up {
                    yValue = self.listOffsetYRoute
                    height = self.view.frame.height - self.listOffsetYRoute
                    mapHeight = self.view.frame.height - self.listRouteController.view.bounds.height
                    self.mapBottomConstraint.constant = self.listRouteController.view.bounds.height
                }
            }
            
            frame.origin.y = yValue
            frame.size.height = height
            self.listRouteController.view.frame = frame
            
            mapFrame.size.height = mapHeight
            self.mapView.frame = mapFrame
            
        }) { (finished) in
            //change state
            if self.panelState == .Default {
                if direction == .up {
                    self.panelState = .Full
                } else if direction == .down {
                    self.panelState = .Compact
                }
            }
            else if self.panelState == .Full {
                if direction == .down {
                    self.panelState = .Default
                }
            }
            else if self.panelState == .Compact {
                if direction == .up {
                    self.panelState = .Default
                    self.mapBottomConstraint.constant = self.listRouteController.view.bounds.height
                }
            }
        }
    }
}
