//
//  ServiceDetailViewController.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 19/7/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit

class ServiceDetailViewController: UIViewController {
    
    @IBOutlet fileprivate weak var serviceNoLbl: UILabel!
    @IBOutlet fileprivate weak var operatorLbl: UILabel!
    
    var busService:BusService! = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Service Detail"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print(busService)
        
        serviceNoLbl.text = busService.ServiceNo
        operatorLbl.text = busService.Operator
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
