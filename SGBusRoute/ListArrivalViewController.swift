//
//  ListViewController.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 16/6/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit

protocol ListArrivalViewControllerDelegate {
    func didTapRefresh(onBusStop busStop:BusStop)
    func didTapArrivalRow(withArrival arrival:ServiceArrival, onBusStop busStop:BusStop)
    func arrivalListDidSwipe(direction:UISwipeGestureRecognizerDirection)
}

class ListArrivalViewController: UIViewController {
    
    enum Operator:String {
        case sbst = "SBST"
        case smrt = "SMRT"
        case tts = "TTS"
        case gas = "GAS"
    }
    
    enum Load:String {
        case sea = "SEA" //seats available
        case sda = "SDA" //standing available
        case lsd = "LSD" //limited standing
    }
    
    enum busType:String {
        case sd = "SD" //single deck
        case dd = "DD" //double deck
        case bd = "BD" //bendy
    }
    
    enum feature:String {
        case wab = "WAB" //wheelchair-accessible bus
    }
    
    // IBOutlets
    @IBOutlet fileprivate weak var headerView: UIView!
    @IBOutlet fileprivate weak var stopIDLbl: UILabel!
    @IBOutlet fileprivate weak var stopDescLbl: UILabel!
    @IBOutlet fileprivate weak var stopRoadNameLbl: UILabel!
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    // private vars
    fileprivate var isLoading = true
    fileprivate var dataArrivals:[ServiceArrival] = []
    fileprivate var busStop:BusStop!
    fileprivate lazy var headerSeparator:UIView = {
        let frame = CGRect(x: 0, y: 0, width: 1, height: 1)
        let v = UIView(frame: frame)
        v.backgroundColor = UIColor.groupTableViewBackground
        return v
    }()
  
    // public vars
    var delegate:ListArrivalViewControllerDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeUpGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture(swipeGesture:)))
        swipeUpGesture.direction = .up
        headerView.addGestureRecognizer(swipeUpGesture)
        
        let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture(swipeGesture:)))
        swipeDownGesture.direction = .down
        headerView.addGestureRecognizer(swipeDownGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if headerSeparator.superview == nil {
            var frame = headerSeparator.frame
            frame.origin = CGPoint(x: 0, y: tableView.frame.origin.y)
            frame.size.width = self.view.bounds.width
            headerSeparator.frame = frame
            self.view.addSubview(headerSeparator)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: - UIGestureRecognizer

extension ListArrivalViewController {
    
    internal func handleSwipeGesture(swipeGesture:UISwipeGestureRecognizer) {
        delegate?.arrivalListDidSwipe(direction: swipeGesture.direction)
    }
}

// MARK: - Data

extension ListArrivalViewController {
    
    func populateArrivals(arrivals:[ServiceArrival], onBusStop busStop:BusStop) {
        
        isLoading = false
        
        self.busStop = busStop
        stopDescLbl.text = busStop.Description

        let string = busStop.BusStopCode + " " + busStop.RoadName
        let attributedString = NSMutableAttributedString(string: string)
        
        if let range = string.range(of: busStop.BusStopCode) {
            let nsRange = string.nsRange(from: range)
            attributedString.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 12.0)],
                                           range: nsRange)
        }
        
        if let range = string.range(of: busStop.RoadName) {
            let nsRange = string.nsRange(from: range)
            attributedString.addAttributes([NSForegroundColorAttributeName: Colors.controlShadowColor,
                                            NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)],
                                           range: nsRange)
        }

        stopIDLbl.attributedText = attributedString
        dataArrivals = arrivals
        tableView.reloadData()
    }
    
    func showLoading(serviceNo:String) {
        
        isLoading = true
        if let row = dataArrivals.index(where: { $0.ServiceNo == serviceNo }) {
            let indexPath = IndexPath(row: row, section: 0)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func hideLoading(serviceNo:String) {
        
        isLoading = false
        if let row = dataArrivals.index(where: { $0.ServiceNo == serviceNo }) {
            let indexPath = IndexPath(row: row, section: 0)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
}

// MARK: - IBActions

extension ListArrivalViewController {
    
    @IBAction func refreshTapped(_ sender: AnyObject) {
        isLoading = true
        tableView.reloadData()
        delegate?.didTapRefresh(onBusStop: busStop)
    }
}

// MARK: - UITableViewDataSource

extension ListArrivalViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArrivals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell-arrival", for: indexPath) as! CellArrival
        let arrival = dataArrivals[indexPath.row]
        cell.serviceNoLbl.text = arrival.ServiceNo
        
        /*
         NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
         attachment.image = [UIImage imageNamed:@"MyIcon.png"];
         
         NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
         
         NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:@"My label text"];
         [myString appendAttributedString:attachmentString];
         
         myLabel.attributedText = myString;
         */
        
        var nextBusEstMins = ""
        if let nextBusArrString = arrival.NextBus?.EstimatedArrival {
            if let nextBusArrDate = Utility.dateFromString(string: nextBusArrString, withFormat: Constants.defaultDateFormat) {
                let nextBusArrMins = Utility.getDiffMinutesFromNow(toDate: nextBusArrDate)
                nextBusEstMins = nextBusArrMins <= 0 ? Strings.Arr : "\(nextBusArrMins)"
            }
            else {
                nextBusEstMins = Strings.NA
            }
        }

        var nextBus2EstMins = ""
        if let subqBusArrString = arrival.NextBus2?.EstimatedArrival {
            if let subqBusArrDate = Utility.dateFromString(string: subqBusArrString, withFormat: Constants.defaultDateFormat) {
                let subqBusArrMins = Utility.getDiffMinutesFromNow(toDate: subqBusArrDate)
                nextBus2EstMins = subqBusArrMins <= 0 ? Strings.Arr : "\(subqBusArrMins)"
            }
            else {
                nextBus2EstMins = Strings.NA
            }
        }
        
        var nextBus3EstMins = ""
        if let subq3BusArrString = arrival.NextBus3?.EstimatedArrival {
            if let subq3BusArrDate = Utility.dateFromString(string: subq3BusArrString, withFormat: Constants.defaultDateFormat) {
                let subq3BusArrMins = Utility.getDiffMinutesFromNow(toDate: subq3BusArrDate)
                nextBus3EstMins = subq3BusArrMins <= 0 ? Strings.Arr : "\(subq3BusArrMins)"
            } else {
                nextBus3EstMins = Strings.NA
            }
        }
        
        let nextBusAttrStr = NSMutableAttributedString(string: nextBusEstMins)
        let attrNextBus = getAttributedColor(onNextBus: arrival.NextBus, estimationMins: nextBusEstMins)
        let rangeNextBus = NSRange(location: 0, length: nextBusEstMins.characters.count)
        nextBusAttrStr.addAttributes(attrNextBus, range: rangeNextBus)
        cell.timeNextLbl.attributedText = nextBusAttrStr
        
        //NextBus2
        let nextBus2AttrStr = NSMutableAttributedString(string: nextBus2EstMins)
        let attrNextBus2 = getAttributedColor(onNextBus: arrival.NextBus2, estimationMins: nextBus2EstMins)
        let rangeNextBus2 = NSRange(location: 0, length: nextBus2EstMins.characters.count)
        nextBus2AttrStr.addAttributes(attrNextBus2, range: rangeNextBus2)
       
        //NextBus3
        let nextBus3AttrStr = NSMutableAttributedString(string: nextBus3EstMins)
        let attrNextBus3 = getAttributedColor(onNextBus: arrival.NextBus3, estimationMins: nextBus3EstMins)
        let rangeNextBus3 = NSRange(location: 0, length: nextBus3EstMins.characters.count)
        nextBus3AttrStr.addAttributes(attrNextBus3, range: rangeNextBus3)
        
        //NextBus2, NextBus3
        let subqBusMins = NSMutableAttributedString()
        let spaceAttr = NSMutableAttributedString(string: ", ")
        
        subqBusMins.append(nextBus2AttrStr)
        subqBusMins.append(spaceAttr)
        subqBusMins.append(nextBus3AttrStr)
        cell.timeSubqLbl.attributedText = subqBusMins
        
        if(isLoading) {
            cell.activityIndicator.startAnimating()
            cell.timeNextLbl.isHidden = true
            cell.timeSubqLbl.isHidden = true
        } else {
            cell.activityIndicator.stopAnimating()
            cell.timeNextLbl.isHidden = false
            cell.timeSubqLbl.isHidden = false
        }
        
        if let load = arrival.NextBus?.Load {
            
            var loadImage = UIImage(named: "load_green")
            var toShow = 1

            switch load {
            case Load.sda.rawValue:
                loadImage = UIImage(named: "load_yellow")
                toShow = 2
            case Load.lsd.rawValue:
                loadImage = UIImage(named: "load_red")
                toShow = 3
            default:
                break
            }

            var counter = 0
            let count = cell.loadView.subviews.count
            let toHide = count - toShow
            for v in cell.loadView.subviews {
                if counter < toHide {
                    v.isHidden = true
                }
                else {
                    v.isHidden = false
                }
                counter += 1
                
                if v is UIImageView {
                    let imgView = v as! UIImageView
                    imgView.image = loadImage
                }
            }
        }
        
        return cell
    }
    
    func getAttributedColor(onNextBus nextBus:Estimation?, estimationMins:String) -> [String:Any] {
        
        var attr = [NSForegroundColorAttributeName: UIColor.black]
        
        if nextBus?.Load == Load.sea.rawValue {
            if estimationMins == Strings.Arr {
                attr = [NSForegroundColorAttributeName: Colors.darkGreen]
            }
        }
        else if nextBus?.Load == Load.sda.rawValue {
            attr = [NSForegroundColorAttributeName: Colors.amber]
        } else if nextBus?.Load == Load.lsd.rawValue {
            attr = [NSForegroundColorAttributeName: UIColor.red]
        }
        
        return attr
    }
}

// MARK: - UITableViewDelegate

extension ListArrivalViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    
        let arrival = dataArrivals[indexPath.row]
        delegate?.didTapArrivalRow(withArrival: arrival, onBusStop: busStop)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 47
    }
}
