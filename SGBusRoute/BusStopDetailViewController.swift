//
//  BusStopDetailViewController.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 19/7/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit

class BusStopDetailViewController: UIViewController {

    @IBOutlet fileprivate weak var stopIDLbl: UILabel!
    @IBOutlet fileprivate weak var stopNameLbl: UILabel!
    @IBOutlet fileprivate weak var roadNameLbl: UILabel!
    
    //public vars
    var busServices:[String] = []
    var busStop:BusStop? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //bus stop
        stopIDLbl.text = busStop?.BusStopCode
        stopNameLbl.text = busStop?.Description
        roadNameLbl.text = busStop?.RoadName
        
        //services
        let margin:CGFloat = 10
        let availWidth = view.bounds.width - margin * 2
        let spaceBetween:CGFloat = 10
        let itemsPerRow = 6
        var yOffset:CGFloat = 50
        
        let widthPerItem = (availWidth - (CGFloat(itemsPerRow) - 1) * spaceBetween) / CGFloat(itemsPerRow)
        for i in 0 ..< busServices.count {
            
            if i % (itemsPerRow) == 0 {
                yOffset += widthPerItem + spaceBetween
            }
            
            let frame = CGRect(x: CGFloat(i % (itemsPerRow)) * (widthPerItem + spaceBetween) + margin, y: yOffset, width: widthPerItem, height: widthPerItem)
            let btn = UIButton(frame: frame)
            btn.setTitle(busServices[i], for: .normal)
            btn.backgroundColor = UIColor.green
            view.addSubview(btn)
        }
        
        print("view subviews count: \(view.subviews.count)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
