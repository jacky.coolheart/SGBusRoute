//
//  ListRouteViewController.swift
//  SGBusRoute
//
//  Created by Jacky Tjoa on 13/7/17.
//  Copyright © 2017 Jacky Tjoa. All rights reserved.
//

import UIKit

protocol ListRouteViewControllerDelegate {
    func didTapMapIcon()
    func didTapServiceDetail(onBusService busService:BusService)
    func didTapRouteRow(withBusStop busStop: BusStop)
    func routeSegmentDidChange(onServiceNo serviceNo:String, toDirection direction:Int)
    func routeListDidSwipe(direction:UISwipeGestureRecognizerDirection)
}

class ListRouteViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet fileprivate weak var serviceNoLbl: UILabel!
    @IBOutlet fileprivate weak var operatorLbl: UILabel!
    @IBOutlet fileprivate weak var segmentControl: UISegmentedControl!
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    //private vars
    fileprivate var roadNames:[String] = []
    fileprivate var dataRoutes:[String:[BusRoute]] = [:]
    fileprivate var busService:BusService! = nil
    
    //public vars
    var delegate:ListRouteViewControllerDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        segmentControl.tintColor = Colors.cyan
        for v in segmentControl.subviews {
            if v is UILabel {
                let lbl = v as! UILabel
                lbl.numberOfLines = 0
            }
        }
        
        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).numberOfLines = 0
        
        let swipeUpGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture(swipeGesture:)))
        swipeUpGesture.direction = .up
        headerView.addGestureRecognizer(swipeUpGesture)
        
        let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture(swipeGesture:)))
        swipeDownGesture.direction = .down
        headerView.addGestureRecognizer(swipeDownGesture)
        
        let image = UIImage(named: "map")?.withRenderingMode(.alwaysTemplate)
        mapBtn.setImage(image, for: .normal)
        mapBtn.tintColor = Colors.cyan
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: - UIGestureRecognizer

extension ListRouteViewController {
    
    internal func handleSwipeGesture(swipeGesture:UISwipeGestureRecognizer) {
        delegate?.routeListDidSwipe(direction: swipeGesture.direction)
    }
}

// MARK: - Data

extension ListRouteViewController {
    
    func populateRoutes(_ routes:[BusRoute], serviceNo:String, direction:Int) {
        
        roadNames.removeAll()
        dataRoutes.removeAll()
        
        for route in routes {
            if let busStop = route.busStop {
                if dataRoutes[busStop.RoadName] == nil {
                    dataRoutes[busStop.RoadName] = [route]
                    roadNames.append(busStop.RoadName)
                } else {
                    dataRoutes[busStop.RoadName]?.append(route)
                }
            }
        }
        
        if let busService = RealmHelper.getBusService(serviceNo: serviceNo) {
            self.busService = busService
            serviceNoLbl.text = serviceNo
            operatorLbl.text = busService.Operator
        }
        
        if routes.count > 0 {
            
            if routes[0].BusStopCode == routes[routes.count - 1].BusStopCode {
                
                if segmentControl.numberOfSegments == 2 {
                    segmentControl.removeSegment(at: 1, animated: true)
                }
                
                //loop service
                if segmentControl.numberOfSegments == 1 {
                    let route = "\(routes[0].busStop?.Description ?? "") <> \(routes[routes.count - 1].busStop?.Description ?? "")"
                    segmentControl.setTitle(route, forSegmentAt: 0)
                    segmentControl.selectedSegmentIndex = 0
                }
                
            } else {
                
                var route1 = ""
                var route2 = ""
                
                if routes[0].Direction == 1 {
                    route1 = "\(routes[0].busStop?.Description ?? "") > \(routes[routes.count - 1].busStop?.Description ?? "")"
                    route2 = "\(routes[routes.count - 1].busStop?.Description ?? "") > \(routes[0].busStop?.Description ?? "")"
                } else {
                    route2 = "\(routes[0].busStop?.Description ?? "") > \(routes[routes.count - 1].busStop?.Description ?? "")"
                    route1 = "\(routes[routes.count - 1].busStop?.Description ?? "") > \(routes[0].busStop?.Description ?? "")"
                }
                
                segmentControl.setTitle(route1, forSegmentAt: 0)
                if segmentControl.numberOfSegments == 1 {
                    segmentControl.insertSegment(withTitle: route2, at: 1, animated: true)
                } else if segmentControl.numberOfSegments == 2 {
                    segmentControl.setTitle(route2, forSegmentAt: 1)
                }
                
                let segmentIndex = direction - 1
                if segmentIndex >= 0 {
                    segmentControl.selectedSegmentIndex = segmentIndex
                }
            }
        }
        
        tableView.setContentOffset(CGPoint.zero, animated: true)
        tableView.reloadData()
    }
    
    func refresh() {
        tableView.reloadData()
    }
}

// MARK: - IBActions

extension ListRouteViewController {
    
    @IBAction func segmentDidChange(_ sender: UISegmentedControl) {
        if(busService != nil) {
            delegate?.routeSegmentDidChange(onServiceNo: busService.ServiceNo, toDirection: sender.selectedSegmentIndex + 1)
        }
    }
    
    @IBAction func mapTapped(_ sender: AnyObject) {
        delegate?.didTapMapIcon()
    }
    
    @IBAction func detailTapped(_ sender: AnyObject) {
        delegate?.didTapServiceDetail(onBusService: busService)
    }
}

// MARK: - UITableViewDataSource

extension ListRouteViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return roadNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let roadName = roadNames[section]
        let routes = dataRoutes[roadName]
        
        return routes != nil ? routes!.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell-route", for: indexPath) as! CellRoute
        
        let roadName = roadNames[indexPath.section]
        let routes = dataRoutes[roadName]
        
        let route = routes?[indexPath.row]
        cell.stopNameLbl.text = route?.busStop?.Description
        cell.stopNoLbl.text = route?.BusStopCode
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let roadName = roadNames[section]
        return roadName
    }
}

// MARK: - UITableViewDelegate

extension ListRouteViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let roadName = roadNames[indexPath.section]
        let routes = dataRoutes[roadName]
 
        if let busStop = routes?[indexPath.row].busStop {
            delegate?.didTapRouteRow(withBusStop: busStop)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
